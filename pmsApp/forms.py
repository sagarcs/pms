from django import forms
from .models import Application, Applicant, Documents

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ('FirstName', 'MiddleName', 'LastName', 'DateOfBirth', 'Gender', 'Address','PlaceOfBirth', 'City', 'State', 'Zipcode')
        labels = {
            "FirstName": "First Name",
            "MiddleName": "Middle Name",
            "LastName": "Last Name",
            "DateOfBirth": "Date Of Birth",
            "Gender": "Gender",
            "Address": "Address",
            "PlaceOfBirth": "Place Of Birth",
        }


class RegisterApplicantForm(forms.ModelForm):
    ConfirmPassword = forms.CharField(label="Confirm Password", widget = forms.PasswordInput(attrs={'class':'form-control'}))
    class Meta:
        model = Applicant
        fields = ('UserName', 'MailId', 'Password', )
        widgets = {
            'Password': forms.PasswordInput(attrs={'class':'form-control'}),
            'UserName': forms.TextInput(attrs={'class':'form-control'}),
            'MailId': forms.TextInput(attrs={'class':'form-control'}),
        }
        labels = {
            "UserName": "User Name",
            "MailId": "Email Id",
        }

class LoginApplicantForm(forms.Form):
    username = forms.CharField(label="Enter Mail Id", widget=forms.TextInput(attrs={'class': 'form-control'}),
                               required=False)
    password = forms.CharField(label="Enter Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))

class LoginAdmin(forms.Form) :
    username = forms.CharField(label="Enter Mail Id", widget=forms.TextInput(attrs={'class': 'form-control'}),
                               required=False)
    password = forms.CharField(label="Enter Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))



class DocumentsForm(forms.ModelForm):
    class Meta:
        model = Documents
        fields = ('Photo', 'AddressProof', 'BirthCertificate')
        labels = {
        "AddressProof": "Address Proof",
        "BirthCertificate": "Birth Certificate"
        }
